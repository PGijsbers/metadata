import typing

import numpy  # type: ignore

from .. import metadata as metadata_module

__all__ = ('ndarray', 'matrix')


class ndarray(numpy.ndarray):
    """
    Extended `numpy.ndarray` with the `metadata` attribute.

    `numpy.ndarray` **does not** allow directly attaching `metadata` attribute to an instance.
    """

    metadata: metadata_module.DataMetadata = None


class matrix(numpy.matrix):
    """
    Extended `numpy.matrix` with the `metadata` attribute.

    `numpy.matrix` **does** allow directly attaching `metadata` attribute to an instance.
    """

    metadata: metadata_module.DataMetadata = None


typing.Sequence.register(numpy.ndarray)  # type: ignore
typing.Sequence.register(numpy.matrix)  # type: ignore
