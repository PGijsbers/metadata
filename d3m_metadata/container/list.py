import typing

from .. import metadata as metadata_module

__all__ = ('List',)

T = typing.TypeVar('T')


class List(typing.List[T]):
    """
    Extended Python standard `typing.List` with the `metadata` attribute.

    You have to create a subclass of this generic type with specified element type before
    you can instantiate lists.

    One can use various base and container types as its elements.
    """

    metadata: metadata_module.DataMetadata = None
