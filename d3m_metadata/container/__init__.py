"""
This module provides various container types one can use to pass values between primitives.
"""

import typing

from .dataset import *
from .list import *
from .numpy import *
from .pandas import *

Container = typing.Union[ndarray, matrix, DataFrame, SparseDataFrame, List, Dataset]
