import collections
import copy
import datetime
import decimal
import json
import os.path
import sys
import typing

import frozendict  # type: ignore
import jsonschema  # type: ignore
from pytypes import type_util  # type: ignore

__all__ = ('ALL_ELEMENTS', 'Metadata', 'DataMetadata', 'PrimitiveMetadata')


class ALL_ELEMENTS_TYPE:
    __slots__ = ()

    def __repr__(self) -> str:
        return 'ALL_ELEMENTS'


ALL_ELEMENTS = ALL_ELEMENTS_TYPE()


# None is not allowed because we are using it to signal a removal of metadata in update.
KNOWN_IMMUTABLE_TYPES = (
    bool, int, complex, decimal.Decimal, float, str, bytes, datetime.date, datetime.time,
    datetime.datetime, datetime.timedelta, datetime.tzinfo, datetime.timezone, type(None)
)


class RefResolverNoRemote(jsonschema.RefResolver):
    def resolve_remote(self, uri: str) -> typing.Any:
        raise NotImplementedError("Remote resolving disabled: {uri}".format(uri=uri))


def load_schema_validators() -> typing.List[jsonschema.Draft4Validator]:
    schemas_path = os.path.join(os.path.dirname(__file__), 'schemas', 'v0')

    with open(os.path.join(schemas_path, 'definitions.json'), 'r') as schema_file:
        definitions_json = json.load(schema_file)

    validators = []

    for schema_filename in ('container.json', 'data.json', 'primitive.json'):
        with open(os.path.join(schemas_path, schema_filename), 'r') as schema_file:
            schema_json = json.load(schema_file)

        jsonschema.Draft4Validator.check_schema(schema_json)

        validator = jsonschema.Draft4Validator(
            schema=schema_json,
            types=dict(jsonschema.Draft4Validator.DEFAULT_TYPES, **{
                'array': (list, tuple, set),
                'object': (dict, frozendict.frozendict, frozendict.FrozenOrderedDict),
                'type': type,
            }),
            resolver=RefResolverNoRemote(schema_json['id'], schema_json, {
                'https://metadata.datadrivendiscovery.org/schemas/v0/definitions.json': definitions_json,
            }),
            format_checker=jsonschema.draft4_format_checker,
        )

        validators.append(validator)

    return validators


CONTAINER_SCHEMA_VALIDATOR, DATA_SCHEMA_VALIDATOR, PRIMITIVE_SCHEMA_VALIDATOR = load_schema_validators()


T = typing.TypeVar('T', bound='Metadata')
SimpleSelectorSegment = typing.Union[int, str]
SelectorSegment = typing.Union[SimpleSelectorSegment, ALL_ELEMENTS_TYPE]
# A list or tuple of integers, strings, or ALL_ELEMENTS.
Selector = typing.Union[typing.List[SelectorSegment], typing.Tuple[SelectorSegment, ...]]


def make_immutable(obj: typing.Any, _path: typing.List[typing.Any] = None) -> typing.Any:
    """
    Converts a given ``obj`` into an immutable copy of it, if possible.

    Parameters
    ----------
    obj : any
        Object to convert.
    _path : list(any)
        Current path during ``obj`` traversal.

    Returns
    -------
    any
        An immutable copy of ``obj``.
    """

    if _path is None:
        _path = [obj]

    if isinstance(obj, KNOWN_IMMUTABLE_TYPES):
        # Because str is among known immutable types, it will not be picked apart as a sequence.
        return obj
    if obj is ALL_ELEMENTS:
        return obj
    if isinstance(obj, type):
        # Assume all types are immutable.
        return obj
    if isinstance(obj, typing.Mapping):
        # We simply always preserve order of the mapping. Because we want to make sure also mapping's
        # values are converted to immutable values, we cannot simply use MappingProxyType.
        return frozendict.FrozenOrderedDict((make_immutable(k, _path + [k]), make_immutable(v, _path + [k])) for k, v in obj.items())
    if isinstance(obj, typing.Set):
        return frozenset(make_immutable(o, _path + [o]) for o in obj)
    if isinstance(obj, tuple):
        # To preserve named tuples.
        return type(obj)(make_immutable(o, _path + [o]) for o in obj)
    if isinstance(obj, typing.Sequence):
        return tuple(make_immutable(o, _path + [o]) for o in obj)

    raise TypeError("{obj} at {path} is not known to be immutable.".format(obj=obj, path=_path))


class MetadataJsonEncoder(json.JSONEncoder):
    def default(self, o: typing.Any) -> typing.Any:
        if isinstance(o, frozendict.FrozenOrderedDict):
            return collections.OrderedDict(o)
        if isinstance(o, type):
            return type_util.type_str(o, assumed_globals={}, update_assumed_globals=False)

        return super().default(o)


class LogEntry(typing.NamedTuple):
    selector: typing.Any
    metadata: typing.Any
    source: typing.Any
    timestamp: typing.Any


class MetadataEntry:
    def __init__(self) -> None:
        self.elements: typing.Dict[SimpleSelectorSegment, MetadataEntry] = {}
        self.all_elements: MetadataEntry = None
        self.metadata: frozendict.FrozenOrderedDict = frozendict.FrozenOrderedDict()


class Metadata:
    """
    A basic class to be used as a value for `metadata` attribute
    on values passed between primitives.

    Instances are immutable.

    Parameters
    ----------
    for_value : Any
        Optional value to associated with metadata and check future updates against to
        make sure they points to data which exists.
    """

    def __init__(self, for_value: typing.Any = None) -> None:
        self._for_value = for_value

        self._metadata_log: typing.Tuple[LogEntry, ...] = ()
        self._current_metadata = MetadataEntry()

        self._hash: int = None

    # TODO: If particular metadata (dict) gets empty ({}), we should remove it from its parent metadata entry.
    def update(self: T, selector: Selector, metadata: typing.Dict[str, typing.Any], *, for_value: typing.Any = None, source: typing.Any = None, timestamp: datetime.datetime = None) -> T:
        """
        Updates metadata with new ``metadata`` for data pointed to with ``selector``.

        It returns a copy of this metadata object with new metadata applied.

        Parameters
        ----------
        selector : Tuple(str or int or ALL_ELEMENTS)
            A selector pointing to data.
        metadata : Dict
            A map of keys and values with metadata.
        for_value : Any
            Optional value to check ``selector`` against to make sure it points to data which exists.
            It replaces any previous set value associated with metadata.
        source : primitive or Any
            A source of this metadata change. Can be an instance of a primitive or any other relevant
            source reference.
        timestamp : datetime
            A timestamp of this metadata change.

        Returns
        -------
        Metadata
            Updated metadata.
        """

        if for_value is None:
            for_value = self._for_value

        parent_metadata_log = self._metadata_log
        parent_current_metadata = self._current_metadata

        self.check_selector(selector, for_value)

        metadata = make_immutable(metadata)

        if not isinstance(metadata, frozendict.FrozenOrderedDict):
            raise TypeError("Metadata should be a dict.")

        if timestamp is None:
            timestamp = datetime.datetime.now(datetime.timezone.utc)

        new_metadata_log_entry = LogEntry(selector=selector, metadata=metadata, source=source, timestamp=timestamp)

        new_metadata = type(self)(for_value)

        new_metadata._metadata_log = parent_metadata_log + (new_metadata_log_entry,)

        new_metadata._current_metadata = new_metadata._update(selector, parent_current_metadata, metadata)

        return new_metadata

    # TODO: Using ALL_ELEMENTS should return only values which really hold for all elements.
    #       Or, a different special selector could be provided for that.
    # TODO: Provide additional special selectors.
    #       For example, to get which elements have metadata which does not hold for all elements.
    # TODO: Allow querying only a subset of metadata (not the whole dict).
    def query(self, selector: Selector) -> frozendict.FrozenOrderedDict:
        """
        Returns metadata for data pointed to with ``selector``.

        When querying using ALL_ELEMENTS means only metadata which has been set using ALL_ELEMENTS
        is returned.

        Parameters
        ----------
        selector : Tuple(str or int or ALL_ELEMENTS)
            A selector to query metadata for.

        Returns
        -------
        frozendict.FrozenOrderedDict
            Metadata at a given selector.
        """

        self.check_selector(selector)

        # TODO: Maybe cache results? LRU?
        return self._query(selector, self._current_metadata)

    def _query(self, selector: Selector, metadata_entry: typing.Optional[MetadataEntry]) -> frozendict.FrozenOrderedDict:
        if metadata_entry is None:
            return frozendict.FrozenOrderedDict()
        if len(selector) == 0:
            return self._merge_metadata(frozendict.FrozenOrderedDict(), metadata_entry.metadata, True)

        segment, selector_rest = selector[0], selector[1:]

        all_elements_metadata = self._query(selector_rest, metadata_entry.all_elements)
        if segment is ALL_ELEMENTS:
            metadata = all_elements_metadata
        elif segment in metadata_entry.elements:
            segment = typing.cast(SimpleSelectorSegment, segment)
            metadata = self._query(selector_rest, metadata_entry.elements[segment])
            metadata = self._merge_metadata(all_elements_metadata, metadata, True)
        else:
            metadata = all_elements_metadata

        return metadata

    def _update(self, selector: Selector, metadata_entry: typing.Optional[MetadataEntry], metadata: frozendict.FrozenOrderedDict) -> MetadataEntry:
        if metadata_entry is None:
            new_metadata_entry = MetadataEntry()
        else:
            new_metadata_entry = copy.copy(metadata_entry)

        if len(selector) == 0:
            # We do not remove None values here because we have to keep them around to know
            # which values we have to remove when merging with all elements metadata.
            new_metadata_entry.metadata = self._merge_metadata(new_metadata_entry.metadata, metadata, False)
            return new_metadata_entry

        segment, selector_rest = selector[0], selector[1:]

        if metadata_entry is not None:
            # We will be changing elements, so if we copied metadata_entry, we have to copy elements as well.
            new_metadata_entry.elements = copy.copy(new_metadata_entry.elements)

        if segment is ALL_ELEMENTS:
            new_metadata_entry.all_elements = self._update(selector_rest, new_metadata_entry.all_elements, metadata)

            # Fields on direct elements have precedence over fields on ALL_ELEMENTS, but we want the last
            # call to update to take precedence. So all fields found in metadata just set on ALL_ELEMENTS
            # are removed from all metadata on direct elements.
            for element_segment, element_metadata_entry in new_metadata_entry.elements.items():
                new_metadata_entry.elements[element_segment] = self._prune(selector_rest, element_metadata_entry, metadata)

        else:
            segment = typing.cast(SimpleSelectorSegment, segment)
            new_metadata_entry.elements[segment] = self._update(selector_rest, new_metadata_entry.elements.get(segment, None), metadata)

        return new_metadata_entry

    def _merge_metadata(self, metadata1: frozendict.FrozenOrderedDict, metadata2: frozendict.FrozenOrderedDict, remove_none: bool = False) -> frozendict.FrozenOrderedDict:
        """
        Merges all fields from ``metadata`` on top of ``metadata1``, recursively.

        If ``remove_none`` is set to ``True``, then all fields with ``None`` values are removed,
        instead of copied over.

        Only dicts are merged recursively, arrays are not.
        """

        # Copy so that we can mutate.
        metadata = collections.OrderedDict(metadata1)

        for name, value in metadata2.items():
            if name in metadata:
                if remove_none and value is None:
                    del metadata[name]
                elif isinstance(metadata[name], frozendict.FrozenOrderedDict) and isinstance(value, frozendict.FrozenOrderedDict):
                    metadata[name] = self._merge_metadata(metadata[name], value, remove_none)
                else:
                    metadata[name] = value
            else:
                if remove_none and value is None:
                    pass
                # We have to recurse only if we have to remove None values.
                elif remove_none and isinstance(value, frozendict.FrozenOrderedDict):
                    metadata[name] = self._merge_metadata(frozendict.FrozenOrderedDict(), value, remove_none)
                else:
                    metadata[name] = value

        return frozendict.FrozenOrderedDict(metadata)

    def _prune(self, selector: Selector, metadata_entry: typing.Optional[MetadataEntry], metadata: frozendict.FrozenOrderedDict) -> MetadataEntry:
        new_metadata_entry = copy.copy(metadata_entry)

        if len(selector) == 0:
            new_metadata_entry.metadata = self._prune_metadata(new_metadata_entry.metadata, metadata)
            return new_metadata_entry

        segment, selector_rest = selector[0], selector[1:]

        new_metadata_entry.elements = copy.copy(new_metadata_entry.elements)

        if segment is ALL_ELEMENTS:
            new_metadata_entry.all_elements = self._prune(selector_rest, new_metadata_entry.all_elements, metadata)

            for element_segment, element_metadata_entry in new_metadata_entry.elements.items():
                new_metadata_entry.elements[element_segment] = self._prune(selector_rest, element_metadata_entry, metadata)

        elif segment in new_metadata_entry.elements:
            segment = typing.cast(SimpleSelectorSegment, segment)
            new_metadata_entry.elements[segment] = self._prune(selector_rest, new_metadata_entry.elements[segment], metadata)

        return new_metadata_entry

    def _prune_metadata(self, metadata1: frozendict.FrozenOrderedDict, metadata2: frozendict.FrozenOrderedDict) -> frozendict.FrozenOrderedDict:
        """
        Removes all fields which are found in ``metadata2`` from ``metadata1``, recursively.

        Values of ``metadata2`` do not matter, except if they are a dict, in which case
        removal is done recursively.
        """

        # Copy so that we can mutate.
        metadata = collections.OrderedDict(metadata1)

        for name, value in metadata2.items():
            if name not in metadata:
                continue

            if isinstance(metadata[name], frozendict.FrozenOrderedDict) and isinstance(value, frozendict.FrozenOrderedDict):
                metadata[name] = self._prune_metadata(metadata[name], value)
            else:
                del metadata[name]

        return frozendict.FrozenOrderedDict(metadata)

    def check(self, for_value: typing.Any) -> None:
        """
        Checks that all metadata has a corresponding data in ``for_value``.
        If not it raises an exception.

        Parameters
        ----------
        for_value : Any
            Value to check against.
        """

        self._check(self._current_metadata, for_value, [])

    def _check(self, metadata_entry: MetadataEntry, for_value: typing.Any, path: typing.List[SimpleSelectorSegment]) -> None:
        if metadata_entry.all_elements is not None:
            try:
                # We should be able to at least compute length at this dimension
                # (to signal that it is a sequence or a map).
                len(for_value)
            except Exception as error:
                raise ValueError("ALL_ELEMENTS set but dimension missing at {path}.".format(path=path)) from error

        for element_segment, element_metadata_entry in metadata_entry.elements.items():
            try:
                element_value = for_value[element_segment]
            except Exception as error:
                raise ValueError("'{element_segment}' at {path} cannot be resolved.".format(element_segment=element_segment, path=path)) from error

            self._check(element_metadata_entry, element_value, path + [element_segment])

    @classmethod
    def _check_for_value(cls, selector: Selector, for_value: typing.Any, path: typing.List[SelectorSegment]) -> None:
        if not selector:
            return

        segment, selector_rest = selector[0], selector[1:]

        if segment is ALL_ELEMENTS:
            for element_value in for_value:
                cls._check_for_value(selector_rest, element_value, path + [segment])
        else:
            try:
                element_value = for_value[segment]
            except Exception as error:
                raise ValueError("'{segment}' at {path} cannot be resolved.".format(segment=segment, path=path)) from error

            cls._check_for_value(selector_rest, element_value, path + [segment])

    @classmethod
    def check_selector(cls, selector: Selector, for_value: typing.Any = None) -> None:
        """
        Checks that a given ``selector`` is a valid selector. If ``selector`` is invalid it raises an exception.

        It checks that it is a tuple or a list and currently we require that all segments of a selector
        are strings, integers, or a special value ``ALL_ELEMENTS``.

        If ``for_value`` is provided, it also tries to resolve the ``selector`` against the value
        to assure that the selector is really pointing to data which exists.

        Parameters
        ----------
        selector : Tuple(str or int or ALL_ELEMENTS)
            Selector to check.
        for_value : Any
            Value to check against.
        """

        if isinstance(selector, list):
            selector = tuple(selector)
        if not isinstance(selector, tuple):
            raise TypeError("Selector is not a tuple or a list.")

        path = []
        for segment in selector:
            path.append(segment)

            if not isinstance(segment, (str, int)) and segment is not ALL_ELEMENTS:
                raise TypeError("'{segment}' at {path} is not a str, int, or ALL_ELEMENTS.".format(segment=segment, path=path))

        if for_value is not None:
            cls._check_for_value(selector, for_value, [])

    def __hash__(self) -> int:
        if self._hash is None:
            self._hash = hash(self._metadata_log)

        return self._hash

    def get_elements(self, selector: Selector) -> typing.List[SelectorSegment]:
        """
        Returns a list of element names which exists under a selector, if any.

        Parameters
        ----------
        selector : Tuple(str or int or ALL_ELEMENTS)
            A selector to return elements under.

        Returns
        -------
        List[int or str or ALL_ELEMENTS]
            List of element names.
        """

        self.check_selector(selector)

        return self._get_elements(selector, self._current_metadata)

    def _get_elements(self, selector: Selector, metadata_entry: typing.Optional[MetadataEntry]) -> typing.List[SelectorSegment]:
        if metadata_entry is None:
            return []
        if len(selector) == 0:
            if metadata_entry.all_elements is not None:
                all_elements: typing.List[SelectorSegment] = [ALL_ELEMENTS]
            else:
                all_elements = []
            return all_elements + list(metadata_entry.elements.keys())

        segment, selector_rest = selector[0], selector[1:]

        all_elements_elements = self._get_elements(selector_rest, metadata_entry.all_elements)
        if segment is ALL_ELEMENTS:
            elements = all_elements_elements
        elif segment in metadata_entry.elements:
            segment = typing.cast(SimpleSelectorSegment, segment)
            elements = self._get_elements(selector_rest, metadata_entry.elements[segment])
            elements = list(set(all_elements_elements + elements))
        else:
            elements = all_elements_elements

        return elements

    def pprint(self, selector: Selector = None, handle: typing.TextIO = None, _level: int = 0) -> None:
        """
        Pretty-prints metadata to ``handle``, or `sys.stdout` if not specified.

        Parameters
        ----------
        selector : Tuple(str or int or ALL_ELEMENTS)
            A selector to start pretty-printing at.
        handle : TextIO
            A handle to pretty-print to. Default is `sys.stdout`.
        """
        if selector is None:
            selector = []

        if handle is None:
            handle = sys.stdout

        self.check_selector(selector)

        selector = list(selector)

        indent = ' ' * _level

        handle.write('{indent}Selector:\n{indent} {selector}\n'.format(indent=indent, selector=tuple(selector)))

        handle.write('{indent}Metadata:\n'.format(indent=indent))
        for line in json.dumps(self.query(selector), indent=1, cls=MetadataJsonEncoder).splitlines():
            handle.write('{indent} {line}\n'.format(indent=indent, line=line))

        elements = self.get_elements(selector)

        if not elements:
            return

        if ALL_ELEMENTS in elements:
            handle.write('{indent}All elements:\n'.format(indent=indent))
            self.pprint(selector + [ALL_ELEMENTS], handle=handle, _level=_level + 1)

        first_element = True
        for element in elements:
            if element is ALL_ELEMENTS:
                continue

            if first_element:
                handle.write('{indent}Elements:\n'.format(indent=indent))
                first_element = False

            self.pprint(selector + [element], handle=handle, _level=_level + 1)


# TODO: Should we automatically extract things from the value which we can?
#       Like dimensions and structural types.
class DataMetadata(Metadata):
    """
    A class for metadata for data values.

    It checks all updates against container and data schemas.
    """

    def update(self: T, selector: Selector, metadata: typing.Dict[str, typing.Any], *, for_value: typing.Any = None, source: typing.Any = None, timestamp: datetime.datetime = None) -> T:
        new_metadata = super().update(selector, metadata, for_value=for_value, source=source, timestamp=timestamp)

        updated_metadata = new_metadata.query(selector)

        if len(selector) == 0:
            CONTAINER_SCHEMA_VALIDATOR.validate(updated_metadata)
        else:
            DATA_SCHEMA_VALIDATOR.validate(updated_metadata)

        return new_metadata


# TODO: Extract additional metadata from the primitive automatically.
class PrimitiveMetadata(Metadata):
    """
    A class for metadata for primitives.

    It checks all updates against primitive schema.
    """

    # Not adhering to Liskov substitution principle: we do not have a selector and
    # selector is not optional in the parent class. But if it was, it would have to
    # be after metadata argument and not be first among arguments.
    def update(self: T, metadata: typing.Dict[str, typing.Any], *, for_value: typing.Any = None, source: typing.Any = None, timestamp: datetime.datetime = None) -> T:  # type: ignore
        new_metadata = super().update((), metadata, for_value=for_value, source=source, timestamp=timestamp)

        updated_metadata = new_metadata.query(())

        PRIMITIVE_SCHEMA_VALIDATOR.validate(updated_metadata)

        return new_metadata
