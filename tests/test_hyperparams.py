import typing
import unittest
from collections import OrderedDict

from d3m_metadata import hyperparams


class TestHyperparams(unittest.TestCase):
    def test_hyperparameter(self):
        hyperparameter = hyperparams.Hyperparameter('nothing', str)

        self.assertEqual(hyperparameter.default, 'nothing')
        self.assertEqual(hyperparameter.sample(42), 'nothing')

        with self.assertRaisesRegex(TypeError, 'Default value \'.*\' is not an instance of the structural type'):
            hyperparams.Hyperparameter('nothing', int)

    def test_enumeration(self):
        hyperparameter = hyperparams.Enumeration(['a', 'b', 1, 2, None], None)

        self.assertEqual(hyperparameter.default, None)
        self.assertEqual(hyperparameter.sample(42), 2)

        with self.assertRaisesRegex(ValueError, 'Default value \'.*\' is not among values'):
            hyperparams.Enumeration(['a', 'b', 1, 2], None)

        with self.assertRaisesRegex(TypeError, 'Value \'.*\' is not an instance of the structural type'):
            hyperparams.Enumeration(['a', 'b', 1, 2, None], None, typing.Union[str, int])

    def test_other(self):
        hyperparameter = hyperparams.UniformInt(1, 10, 2)

        self.assertEqual(hyperparameter.default, 2)
        self.assertEqual(hyperparameter.sample(42), 7)

        with self.assertRaisesRegex(ValueError, 'Default value \'.*\' is outside of range'):
            hyperparams.UniformInt(1, 10, 0)

        hyperparameter = hyperparams.Uniform(1.0, 10.0, 2.0)

        self.assertEqual(hyperparameter.default, 2.0)
        self.assertEqual(hyperparameter.sample(42), 4.370861069626263)

        with self.assertRaisesRegex(ValueError, 'Default value \'.*\' is outside of range'):
            hyperparams.Uniform(1.0, 10.0, 0.0)

        hyperparameter = hyperparams.LogUniform(1.0, 10.0, 2.0)

        self.assertEqual(hyperparameter.default, 2.0)
        self.assertEqual(hyperparameter.sample(42), 79.111723081473684)

        with self.assertRaisesRegex(ValueError, 'Default value \'.*\' is outside of range'):
            hyperparams.LogUniform(1.0, 10.0, 0.0)

    def test_union(self):
        hyperparameter = hyperparams.Union(
            OrderedDict(
                none=hyperparams.Hyperparameter(None),
                range=hyperparams.UniformInt(1, 10, 2)
            ),
            'none',
        )

        self.assertEqual(hyperparameter.configuration['none'].name, 'none')

        self.assertEqual(hyperparameter.default, None)
        self.assertEqual(hyperparameter.sample(45), 4)

        with self.assertRaisesRegex(TypeError, 'Hyper-parameter name is not a string'):
            hyperparams.Union(OrderedDict({1: hyperparams.Hyperparameter(None)}), 1)

        with self.assertRaisesRegex(TypeError, 'Hyper-parameter description is not an instance of the Hyperparameter class'):
            hyperparams.Union(OrderedDict({'none': None}), 'none')

        with self.assertRaisesRegex(ValueError, 'Default value \'.*\' is not in configuration'):
            hyperparams.Union(OrderedDict({'range': hyperparams.UniformInt(1, 10, 2)}), 'none')

    def test_hyperparams(self):
        class TestHyperparams(hyperparams.Hyperparams):
            a = hyperparams.Union(OrderedDict(
                range=hyperparams.UniformInt(1, 10, 2),
                none=hyperparams.Hyperparameter(None),
            ), 'range')
            b = hyperparams.Uniform(1.0, 10.0, 2.0)

        self.assertEqual(TestHyperparams.configuration['a'].name, 'a')

        self.assertEqual(TestHyperparams.defaults(), {'a': 2, 'b': 2.0})
        self.assertEqual(TestHyperparams.defaults(), TestHyperparams({'a': 2, 'b': 2.0}))
        self.assertEqual(TestHyperparams.sample(42), {'a': 4, 'b': 9.556428757689245})
        self.assertEqual(TestHyperparams.sample(42), TestHyperparams({'a': 4, 'b': 9.556428757689245}))

        test_hyperparams = TestHyperparams({'a': TestHyperparams.configuration['a'].default, 'b': TestHyperparams.configuration['b'].default})

        self.assertEqual(test_hyperparams['a'], 2)
        self.assertEqual(test_hyperparams['b'], 2.0)

        with self.assertRaisesRegex(ValueError, 'Not all hyper-parameters are specified'):
            TestHyperparams({'a': TestHyperparams.configuration['a'].default})

        with self.assertRaisesRegex(ValueError, 'Additional hyper-parameters are specified'):
            TestHyperparams({'a': TestHyperparams.configuration['a'].default, 'b': TestHyperparams.configuration['b'].default, 'c': 'two'})

        TestHyperparams({'a': 3, 'b': 3.0})
        TestHyperparams({'a': None, 'b': 3.0})

        test_hyperparams = TestHyperparams(a=None, b=3.0)
        self.assertEqual(test_hyperparams['a'], None)
        self.assertEqual(test_hyperparams['b'], 3.0)

        with self.assertRaisesRegex(ValueError, 'Value \'.*\' for hyper-parameter \'.*\' has not validated with any of configured hyper-parameters'):
            TestHyperparams({'a': 0, 'b': 3.0})

        with self.assertRaisesRegex(ValueError, 'Value \'.*\' for hyper-parameter \'.*\' is outside of range'):
            TestHyperparams({'a': 3, 'b': 100.0})


if __name__ == '__main__':
    unittest.main()
